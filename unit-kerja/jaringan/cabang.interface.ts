export interface CabangInterface {
    cabang: string
    area: string
    region: string
    status: string
}

const data: CabangInterface[] = [
    {
        "cabang": "KC BANDA ACEH DIPONEGORO",
        "area": "Area Aceh",
        "region":  "RO 1 Aceh",
        "status" : "APPROVED" //Minta Huruf Besar
    }
]