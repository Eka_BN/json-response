export interface AreaInterface {
    area: string
    region: string
    status: string
}

const data: AreaInterface[] = [
    {
        "area": "Area Aceh",
        "region": "RO Aceh",
        "status":  "APPROVED" //Minta Huruf Besar
    }
]