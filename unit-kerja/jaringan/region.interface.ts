export interface RegionInterface {
    region: string
    jumlahArea: number
    jumlahKcKcp: number
    status: string
}

const data: RegionInterface[] = [
    {
        "region": "Region",
        "jumlahArea": 20,
        "jumlahKcKcp": 280,
        "status":  "APPROVED" //Minta Huruf Besar
    }
]