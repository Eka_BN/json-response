export interface UserDelegateInterface {
    nipPengusul: string
    userPengusul: string
    nipPengganti: string
    userPengganti: string
    role: string
    statusDelegasi: string
    startData: string
    endData: string
    status: string
}

const data: UserDelegateInterface[] = [
    {
        nipPengusul: "1244123",
        userPengusul: "Rumi Sitanggang",
        nipPengganti: "1244123",
        userPengganti: "Arumi Sidewingi",
        role: "OPR (Super Admin)",
        statusDelegasi : "Sementara",
        startData: "12/08/2022",
        endData: "12/11/2022",
        status: "APPROVED" //Minta Huruf Besar
    }
]
  